#include <pebble.h>
#include "messaging.h"

#define NUM_MENU_SECTIONS 1
#define NUM_FIRST_MENU_ITEMS 3

static Window *window;
static Window *appWindow;
static TextLayer *text_layer;

static SimpleMenuLayer *simple_menu_layer;
static SimpleMenuSection menu_sections[NUM_MENU_SECTIONS];
static SimpleMenuItem first_menu_items[NUM_FIRST_MENU_ITEMS];

enum { FIRST_APP_KEY, SECOND_APP_KEY, THIRD_APP_KEY };

#define KEY 64
#define MAX_LIST_ITEMS (10)
#define MAX_TEXT_LENGTH (16)

typedef struct {
    char text[MAX_TEXT_LENGTH];
} AppLauncherItem;

static AppLauncherItem app_list[MAX_LIST_ITEMS];
static int active_item_count = 0;

static AppLauncherItem* get_item_at_index(int index) {
    if(index < 0 || index >= MAX_LIST_ITEMS) {
        return NULL;
    }

    return &app_list[index];
}

static void app_list_append(char *data) {
    if(active_item_count == MAX_LIST_ITEMS)
        return;

    strcpy(app_list[active_item_count].text, data);
    active_item_count++;
}


static void appWindow_load(Window *window) {
    for(int i = 0; i < active_item_count; i++) {
        first_menu_items[i] = (SimpleMenuItem){
            .title = get_item_at_index(i)->text,};
    }

  // Bind the menu items to the corresponding menu sections
  menu_sections[0] = (SimpleMenuSection){
    .num_items = active_item_count,
    .items = first_menu_items,
  };

  Layer *appWindow_layer = window_get_root_layer(window);
  GRect bounds = layer_get_frame(appWindow_layer);

  // Initialize the simple menu layer
  simple_menu_layer = simple_menu_layer_create(bounds, window, menu_sections, NUM_MENU_SECTIONS, NULL);

  // Add it to the window for display
  layer_add_child(appWindow_layer, simple_menu_layer_get_layer(simple_menu_layer));

}

static void appWindow_unload(Window *window) {
  text_layer_destroy(text_layer);
}

static void select_click_handler(ClickRecognizerRef recognizer, void *context) {
  appWindow = window_create();
  //window_set_click_config_provider(appWindow, click_config_provider);
  window_set_window_handlers(appWindow, (WindowHandlers) {
    .load = appWindow_load,
    .unload = appWindow_unload,
  });
  const bool animated = true;
  window_stack_push(appWindow, animated);
}

static void up_click_handler(ClickRecognizerRef recognizer, void *context) {
  text_layer_set_text(text_layer, "Up");
  send_int(SEND_ID, 3);
}

static void down_click_handler(ClickRecognizerRef recognizer, void *context) {
  text_layer_set_text(text_layer, "Down");
  
}

static void click_config_provider(void *context) {
  window_single_click_subscribe(BUTTON_ID_SELECT, select_click_handler);
  window_single_click_subscribe(BUTTON_ID_UP, up_click_handler);
  window_single_click_subscribe(BUTTON_ID_DOWN, down_click_handler);
}

//appmessage handlers
static void in_received_handler(DictionaryIterator* iter, void* context){
    Tuple *app_tuple = dict_find(iter, KEY);
    if (app_tuple) {
        app_list_append(app_tuple->value->cstring);
    }
}

static void out_failed_handler(DictionaryIterator *failed, AppMessageResult reason, void *context)
{

  text_layer_set_text(text_layer, "Failed");
}

static void accel_tap_handler(AccelAxisType axis, int32_t direction) {
  //Send a message to computer to wake up
  text_layer_set_text(text_layer, "Select -> to launch app");
}

static void window_load(Window *window) {
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  text_layer = text_layer_create((GRect) { .origin = { 0, 72 }, .size = { bounds.size.w, 20 } });
  text_layer_set_text(text_layer, "Tap to wake up");
  text_layer_set_text_alignment(text_layer, GTextAlignmentCenter);
  layer_add_child(window_layer, text_layer_get_layer(text_layer));
}

static void window_unload(Window *window) {
  text_layer_destroy(text_layer);
}

static void init(void) {
  window = window_create();
  window_set_click_config_provider(window, click_config_provider);
  window_set_window_handlers(window, (WindowHandlers) {
    .load = window_load,
    .unload = window_unload,
  });

  app_message_register_inbox_received(in_received_handler);
  
  const int inbound_size = 64;
  const int outbound_size = 64;
  app_message_open(inbound_size, outbound_size);
  

  const bool animated = true;
  window_stack_push(window, animated);

  accel_tap_service_subscribe(&accel_tap_handler);
}

static void deinit(void) {
  accel_tap_service_unsubscribe();
  window_destroy(window);
}

int main(void) {
  init();

  APP_LOG(APP_LOG_LEVEL_DEBUG, "Done initializing, pushed window: %p", window);
  send_int(REQUEST_APPS, 0);
  app_event_loop();
  deinit();
}
