#include "messaging.h"

void send_int(uint32_t type, uint32_t int_val) {
    DictionaryIterator *iter;

    // Open up the message outbox for writing
    app_message_outbox_begin(&iter);

    // iterator will be null on failure, so bail
    if(iter == NULL)
        return;

    dict_write_uint32(iter, type, int_val);
    dict_write_end(iter);

    // Sends the outbound dictionary
    app_message_outbox_send();
}
